WiFi Charts Module -- By Jiabo Hou
Version: 1.0dev
Created: 2014/05/06
Last Updated: 2014/05/22


||===============================||
||  About                        ||
||===============================||

SUMMARY

The WiFi Charts Module is a Drupal module that displays charts, 
graphs, and visualizations of the University of Waterloo Campus 
wireless internet data collected from AirWave. 

DETAILS:

WiFi Charts pulls data from AirWave every 60 mins (or every cron 
task) using the AirWave API. The data pulled from AirWave is then 
outputted into a flat file named air_wave_data.txt. This data is 
parsed and inserted into a database where it is queried dynamically 
by Drupal when a user visits a WiFi Charts web page. The steps are 
detailed below, along with the file that carries out that process.

STEP 1: Pull data from AirWave, return associative array
  (wifi_charts.includes/air_wave_info.php)

STEP 2: Insert parsed data into external database
  (wifi_charts.module)

STEP 3: Query the external database on web page load
  (includes/wifi_charts.page_queries.inc) // Charts
  (includes/wifi_charts.building_select.inc) // Google Map Markers

STEP 4: Create charts
  (includes/wifi_charts.page_content.inc)

||===============================||
||  Charts                       ||
||===============================||

WiFi Charts provides the following charts:

UW Campus Wireless Statistics:
  - Client Count                 ---            Current/Over Time
  - Download Rate (in Mbps)      ---            Current/Over Time
  - Upload Rate   (in Mbps)      ---            Current/Over Time
  - Device OS Breakdown          ---            Day Before, Spanning
      24 hours
  - Connection Mode Breakdown    ---            Day Before, Spanning
      24 hours
  - RADIUS Authentication Issues ---            Current/Over Time, Per Day
  - Total Number of Rogues       ---            Current/Over Time

UW Campus Per Building Wireless Statistics
  - Client Count                 ---             Current/Over Time
  - Download Rate (in Mbps)      ---             Current/Over Time
  - Upload Rate   (in Mbps)      ---             Current/Over Time
  - Device OS Breakdown          ---            Day Before, Spanning
      24 hours
  - Connection Mode Breakdown    ---            Day Before, Spanning
      24 hours

||==============================||
||  Administration              ||
||==============================||

WiFi Charts adds a new permission for accessing the WiFi Charts configuration
page in Drupal. Options include:

Enable Rogue Chart (Boolean):
  When unchecked disable all information on rogues.

Delete WiFi Charts data older than (Integer):
  When set to a value other than 0, will delete any data older than that many
days.

When to start displaying data (Integer):
When to start displaying data (Cover Page) (Integer):
  Set this number to the number of days of data WiFi charts should display.

||==============================||
||      Files                   ||
||==============================||

uw_wifi_charts.info:
  Drupal module metadata file.

uw_wifi_charts.install:
  Initialize variables on install and delete on uninstall.

uw_wifi_charts.module:
  Implements hook_menu for displaying custom webpages, hook_cron and 
  hook_cron_queue_info for scheduled tasks such as getting new 
  data and database maintenance.

includes/uw_wifi_charts.admin.inc:
  Implements hook_permission to add new administration permission and
  hook_form for the settings page.
  This file contains the page content for the WiFi Charts Settings page.

includes/uw_wifi_charts.building_select.inc:
  Displays a Google Map of the uWaterloo Campus with map markers to
  indicate builidng locations and displays current data for each marker,
  as well as a hyper link to view more detailed data.

includes/uw_wifi_charts.constants.inc:
  Contains global constants used across multiple files, store here for 
  convenience. Also contains a function to connect to the WiFi Charts
  database.

includes/uw_wifi_charts.page_content.inc:
  Creates the chart and markup renderables for the data visualizations.

includes/uw_wifi_charts.page_queries.inc:
  Queries the database to get network data which gets formatted so that
  the Charts module can use the data.

includes/uw_wifi_charts.page_callbacks.inc:
  Displays all the pages that contain charts and tables.
  
styles/uw_wifi_charts.table_align_right.css:
  Aligns columns to the right except for the left most column (which is centered).
  
styles/uw_wifi_charts.no_link_decor.css:
  Changes colour of anchors to default black.
