<?php

/**
 * @file
 * This file contains functions that create renderable content
 * (Charts and markup given appropriate information. )
 */

require_once 'uw_wifi_charts.constants.inc';

/**
 * Creates links at the top of the page to the
 * WiFi Charts cover page, UWaterloo campus data, Index of buildings.
 * Disables the appropriate link if user is currently on that page.
 */
function top_navigation_links() {

  $key_campus_data     = 'campus_data';
  $key_buildings_index = 'buildings_index';

  $content                       = array();
  $content['above_links_desc'] = markup_content(WifiChartsConstants::LABEL_LINK_ABOVE_DESC, array(), '<p>', '</p>');
  $content[$key_campus_data]     = create_hyperlink_content(
    sprintf(WifiChartsConstants::LINK_CHARTS_BUILDING, WifiChartsConstants::B_ID_UW), WifiChartsConstants::LABEL_TITLE_CHARTS_UW, '<ul><li>', '</li>');
  $content[$key_buildings_index] = create_hyperlink_content(
    WifiChartsConstants::LINK_BUILDING_INDEX, WifiChartsConstants::LABEL_TITLE_BUILDING_INDEX, '<li>', '</li>');
  $content['core_links_stats'] = create_hyperlink_content(
    WifiChartsConstants::LINK_CORE_LINKS, WifiChartsConstants::LABEL_LINK_CORE_LINKS, '<li>', '</li>');
  $content['wired_network_stats'] = create_hyperlink_content(
    WifiChartsConstants::LINK_WIRED_NETWORK, WifiChartsConstants::LABEL_LINK_WIRED_NETWORK, '<li>', '</li>');
  $content['wireless_network_stats'] = create_hyperlink_content(
    WifiChartsConstants::LINK_WIRELESS_NETWORK, WifiChartsConstants::LABEL_LINK_WIRELESS_NETWORK, '<li>', '</li>');
  $content['wifi_charts'] = create_hyperlink_content(
    WifiChartsConstants::LINK_COVER_PAGE, WifiChartsConstants::LABEL_LINK_WIFI_CHARTS, '<li>', '</li></ul><br />');

  $curr_path = request_path();

  // If the user is currently on one of these pages, disable that specified link.
  if ($curr_path === sprintf(WifiChartsConstants::LINK_CHARTS_BUILDING, WifiChartsConstants::B_ID_UW)) {
    $content[$key_campus_data] = markup_content(WifiChartsConstants::LABEL_TITLE_CHARTS_UW, array(), '<ul><li>', '</li>');
  }
  elseif ($curr_path === WifiChartsConstants::LINK_BUILDING_INDEX) {
    $content[$key_buildings_index] = markup_content(WifiChartsConstants::LABEL_TITLE_BUILDING_INDEX, array(), '<li>', '</li>');
  }
  elseif ($curr_path === WifiChartsConstants::LINK_COVER_PAGE) {
      $content['wifi_charts'] = markup_content(WifiChartsConstants::LABEL_LINK_WIFI_CHARTS, array(), '<li>', '</li></ul><br />');
  }

  return $content;
}

/**
 * Converts an integer that represents bits/second to a more
 * human readable format of type string.
 * Gbps if $speed > 1073741824 bps (or 1 Gbit exact)
 * Mbps if $speed > 1048576 bps (or 1 Mbit exact)
 * kbps otherwise.
 *
 * @param $speed
 *   The transfer rate as an integer in bits/second.
 *
 * @return: A string that describes the data rate in a nicer format.
 */
function data_rate_prettify($speed) {
  if ($speed >= WifiChartsConstants::GIGABIT) {
    return (string) round(($speed / WifiChartsConstants::GIGABIT), 3) . '&nbsp;' . WifiChartsConstants::GBPS;
  }
  elseif ($speed >= WifiChartsConstants::MEGABIT) {
    return (string) round(($speed / WifiChartsConstants::MEGABIT), 3) . '&nbsp;' . WifiChartsConstants::MBPS;
  }
  elseif ($speed >= WifiChartsConstants::KILOBIT) {
    return (string) round(($speed / WifiChartsConstants::KILOBIT), 3) . '&nbsp;' . WifiChartsConstants::KBPS;
  }
  else {
    return (string) $speed . '&nbsp;' . WifiChartsConstants::BPS;
  }
}

/**
 * Text to display any given data.
 * What is displayed: $label . ': ' . $data.
 *
 * @param $data
 *   The data (dynamic) you want to display.
 * @param $label
 *   The label you want to give it.
 * @param $prefix
 *   HTML to append at the beginning.
 * @param $suffix
 *   HTML to append at the end.
 *
 * @return: a markup renderable.
 */
function markup_content($label, $args = array(), $prefix = '', $suffix = '<br />') {

  $content = array(
    '#type'   => 'markup',
    '#markup' => t(check_plain($label), $args),
    '#prefix' => $prefix,
    '#suffix' => $suffix,
  );
  return $content;
}

/**
 * Text to display the date last updated.
 *
 * @param $datetime
 *   The datetime.
 * @param $prefix
 *   HTML to append at the beginning.
 * @param $suffix
 *   HTML to append at the end.
 *
 * @return: a markup renderable.
 */
function date_last_updated_content($datetime, $prefix = '', $suffix = '<br />') {

  $content = array(
    '#type' => 'markup',
    '#markup' => t(WifiChartsConstants::LABEL_LAST_UPDATED,
      array(WifiChartsConstants::LABEL_PLACEHOLDER_LAST_UPDATED => $datetime)
    ),
    '#prefix' => $prefix . '<em>',
    '#suffix' => '</em>' . $suffix,
  );
  return $content;
}

/**
 * Add hyperlink to a page.
 *
 * @param $url
 *   Where you want the hyperlink to point.
 * @param $pagename
 *   The label of the link you want to give it.
 * @param $prefix
 *   HTML to append at the beginning.
 * @param $suffix
 *   HTML to append at the end.
 *
 * @return: a markup renderable.
 */
function create_hyperlink_content($url, $page_name, $prefix = '', $suffix = '<br />') {
  $content = array(
    '#type'   => 'markup',
    '#markup' => l(t($page_name), $url),
    '#prefix' => $prefix,
    '#suffix' => $suffix,
  );
  return $content;
}

/**
 * Line chart for displaying one data set over time.
 *
 * @param $id
 *   The machine name for the chart.
 *
 * @param $title
 *   The title of the chart (wrapped in t funciton)
 *
 * @param $x_axis_dataset
 *   An associative array with keys:
 *     data => An array of data.
 *     title => The title describing the data.
 *
 * @param $y_axis_datasets
 *   An array of datasets. (see format of $x_axis_dataset for a dataset).
 *
 * @return: a chart renderable.
 */
function line_chart_content($id, $title, $x_axis_dataset, $y_axis_datasets, $aria_label, $aria_describedby) {
  // Input the data into render arrays.
  $chart = array(
    '#type'       => 'chart',
    '#id'         => $id,
    '#chart_type' => 'line',
    '#title'      => $title,
    '#legend_position' => 'bottom',
    '#height'     => WifiChartsConstants::CHART_LINE_HEIGHT,
    '#width'      => WifiChartsConstants::CHART_LINE_WIDTH,
    '#attributes'    => array(
      'aria-label'       => $aria_label,
      'aria-describedby' => $aria_describedby,
      'role'             => 'img',
    ),
  );
  $chart['xaxis'] = array(
    '#axis_type' => 'datetime',
    '#type'      => 'chart_xaxis',
    '#title'     => $x_axis_dataset['title'],
    '#labels'    => $x_axis_dataset['data'],
    '#labels_font_size' => WifiChartsConstants::CHART_LINE_LABEL_FONT_SIZE,
  );
  foreach ($y_axis_datasets as $dataset) {
    $chart[$dataset['title']] = array(
      '#type'          => 'chart_data',
      '#title'         => $dataset['title'],
      '#data'          => $dataset['data'],
      '#marker_radius' => 1.5,
      '#line_width'    => 1.5,
    );
  }
  return $chart;
}

/**
 * Displays a pie chart.
 *
 * @param $dataset
 *   data format:
 *     array(
 *       array('label_data_value_1', data_value_1),
 *       array('lable_data_value_2', data_value_2),
 *       ...)
 * @param $label
 *   label of the chart.
 *
 * @return: A pie chart renderable.
 */
function pie_chart_content($id, $title, $dataset, $aria_label, $aria_describedby) {
  $chart = array(
    '#id'            => $id,
    '#type'          => 'chart',
    '#chart_library' => 'google',
    '#chart_type'    => 'pie',
    '#title'         => t(check_plain($title)),
    '#data_labels'   => TRUE,
    '#height'        => WifiChartsConstants::CHART_PIE_HEIGHT,
    '#width'         => WifiChartsConstants::CHART_PIE_WIDTH,
    '#attributes'    => array(
      'aria-label'       => $aria_label,
      'aria-describedby' => $aria_describedby,
      'role'             => 'img',
    ),
  );
  $chart['pie_data'] = array(
    '#type'  => 'chart_data',
    '#title' => t(check_plain($title)),
    '#data'  => $dataset,
  );
  return $chart;
}

/**
 * Renderable array for a table.
 * See https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7 for more information.
 *
 * @param $id
 *   The table id. Used for anchor links.
 * @param $title
 *   The title of the table.
 * @param $header
 *   An array of strings for the column headers.
 * @param $rows
 *   An indexed array of rows of data corresponding to the column headers.
 * @param $col_groups
 *   Used to apply attributes to groups of columns. An array of column groups.
 *
 * @return: A renderable array for a table.
 */
function data_table_content($id, $title, $header, $rows, $class, $col_groups = array()) {
  /* // EXAMPLE OF PARAMETER DATA.
  $header = array(
  t('Animal'),
  array(
  'data' => t('Size'),
  ),
  array(
  'data' => t('Diet'),
  ),
  array(
  'data' => t('Some other column name'),
  ),
  );
  $rows = array(
  array(
  'data' => array(
  'Bear',
  'Big',
  'Omnivore',
  1,
  ),
  'no_striping' => TRUE,
  ),
  array(
  'data' => array(
  array('data' => 'Human'),
  array('data' => 'Medium'),
  array('data' => 'Omnivore'),
  array('data' => 1),
  ),
  'no_striping' => TRUE,
  ),
  array(
  'data' => array(
  array('data' => 'Mouse'),
  array('data' => 'Small'),
  array('data' => 'Herbivore'),
  array('data' => 1),
  ),
  'no_striping' => TRUE,
  ),
  );
   */
  if (empty($class)) {
    $class = WifiChartsConstants::TABLE_DEFAULT_CLASS;
  }
  $table = array(
    'header' => $header,
    'rows'   => $rows,
    'attributes' => array(
      'id' => $id,
      'class' => $class,
    ),
    'sticky' => FALSE,
    'empty' => t(WifiChartsConstants::TABLE_NO_DATA_TEXT),
    'caption' => $title,
    'colgroups' => $col_groups,
  );
  $table_markup = array(
    '#markup' => theme_table($table),
  );

  return $table_markup;
}

/**
 * Wraps each row in $rows  in another array for drupal theme_table.
 * Applies no_striping by default.
 *
 * @param $rows
 *   an indexed array of rows (which is also an indexed array of data).
 *
 * @return array
 *   An array of arrays with keys:
 *     'data' => An array of data.
 *     'no_striping' => TRUE for no bg colour, FALSE for grey bg.
 */
function arr_to_drupal_table_arr($rows, $no_striping = TRUE) {
  $new_rows = array();
  foreach ($rows as $row) {
    $new_rows[] = array(
      'data' => $row,
      'no_striping' => $no_striping,
    );
  }
  return $new_rows;
}

/**
 * Markup renderable for a description of how the data is updated.
 *
 * @return: A markup renderable.
 */
function about_label() {
  $markup['about_info'] = array(
    '#type'   => 'markup',
    '#markup' => t(WifiChartsConstants::LABEL_ABOUT_CHARTS),
    '#prefix' => '<br /><p><em>',
    '#suffix' => '</em></p>',
  );
  return $markup;
}

/**
 * Markup renderable for a description of how rogue data is updated.
 *
 * @return: A markup renderable.
 */
function about_label_rogues() {
  $markup['about_info_rogues'] = array(
    '#type'   => 'markup',
    '#markup' => t(WifiChartsConstants::LABEL_ABOUT_ROGUES),
    '#prefix' => '<br /><p><em>',
    '#suffix' => '</em></p>',
  );
  return $markup;
}

/**
 * Creates a link to an anchor.
 *
 * @param $anchor_name
 *   The name of the anchor to href.
 * @param $label_name
 *   The name of the link that the user sees.
 * @param $prefix
 *   HTML to append at the beginning.
 * @param $suffix
 *   HTML to append at the end.
 */
function ref_anchor($anchor_name, $label_name, $prefix = '', $suffix = '<br />', $class = '') {
  $content = array(
    '#type'   => 'markup',
    '#markup' => $label_name,
    '#prefix' => $prefix . '<a href="#' . $anchor_name . '" class="' . $class . '">',
    '#suffix' => '</a>' . $suffix,
  );
  return $content;
}

/**
 * Creates an unordered list of links to anchors.
 *
 * @param $anchor_links
 *   An array of arrays with keys:
 *     'name' => $name_of_anchor_seen_on_webpage,
 *     'id'   => $anchor_you_want_to_go_to_no_hash
 *
 * @return
 *   A markup renderable of a list of anchors.
 */
function anchor_link_list($anchor_links) {
  $content['anchor_link_list_label'] = array(
    '#type'   => 'markup',
    '#markup' => t(WifiChartsConstants::ANCHOR_JUMP_TO_LABEL),
    '#prefix' => '<br /><div id="' . WifiChartsConstants::ANCHOR_LIST . '">',
    '#suffix' => '</div>',
  );
  foreach ($anchor_links as $key => $info) {
    // Move cursor to last item of array.
    reset($anchor_links);
    if ($key === key($anchor_links)) {
      // First element in the jump list.
      $content[WifiChartsConstants::KEY_ANCHOR_LINKS_PREFIX . $key] = ref_anchor($info['id'], $info['name'], '<ul> <li>', '</li>');
      continue;
    }
    // Move cursor to last item of array.
    end($anchor_links);
    if ($key === key($anchor_links)) {
      // Last element in the jump list.
      $content[WifiChartsConstants::KEY_ANCHOR_LINKS_PREFIX . $key] = ref_anchor($info['id'], $info['name'], '<li>', '</li> </ul>');
      break;
    }
    $content[WifiChartsConstants::KEY_ANCHOR_LINKS_PREFIX . $key] = ref_anchor($info['id'], $info['name'], '<li>', '</li>');
  }
  return $content;
}

/**
 * Markup to show when JS is disabled.
 */
function no_js_markup() {
  $content = array(
    '#type' => 'markup',
    '#markup' => t(WifiChartsConstants::LABEL_NO_JS),
    '#prefix' => '<p> <noscript> <em>',
    '#suffix' => '</em> </noscript> </p>',
  );

  return $content;
}

/**
 * Used to display a page not found.
 */
function display_404() {
  // Just in case db was not set back to default.
  db_set_active();
  // Display drupal 404 page.
  drupal_not_found();
  // Run all exit hooks before running Drupal's exit.
  module_invoke_all('exit');
  exit();
}

/**
 * Creates markup for network data if failed data pull, or if result set is empty.
 *
 * @param $content_arr_labels
 *   An array of labels keyed by their render array keys. This can be
 *   determined by looking at the keys in $content in page_queries.inc.
 */
function empty_result_set_markup($content_arr_labels) {
  $content = array();
  foreach ($content_arr_labels as $key => $value) {
    if (is_array($value)) {
      if (strpos($key, 'chart') !== FALSE) {
        // Capitalize the first letter of every $value['text'].
        $title_text = drupal_strtoupper(drupal_substr($value['text'], 0, 1)) . drupal_substr($value['text'], 1);

        $content[$key][] = markup_content(
          WifiChartsConstants::LABEL_NO_DATA_CHART_TITLE,
          array(WifiChartsConstants::LABEL_PLACEHOLDER_NO_DATA_CHART_TITLE => $title_text),
          '<h3 id="' . $value['id'] . '">', '</h3>'
        );
        $content[$key][] = markup_content(
          WifiChartsConstants::LABEL_NO_DATA_CHART,
          array(WifiChartsConstants::LABEL_PLACEHOLDER_NO_DATA_CHART => $value['text']),
          '', '<br />'
        );
      }
      elseif (strpos($key, 'data') !== FALSE) {
        $content[$key] = data_table_content(
          $value['id'], $value['text'], $value['header'], arr_to_drupal_table_arr(array()), WifiChartsConstants::TABLE_DEFAULT_CLASS);
      }
    }
    // Check if key contains _current, meaning it is a label.
    elseif (strpos($key, WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX) !== FALSE) {
      $value_expl = explode(' ', $value);
      $placeholder = '';
      $placeholder_arr = array();
      foreach ($value_expl as $word) {
        $drupal_placeholders = WifiChartsConstants::getDrupalTPlaceholders();
        if (in_array(drupal_substr($word, 0, 1), $drupal_placeholders)) {
          $placeholder = $word;
          break;
        }
      }
      if (!empty($placeholder)) {
        $placeholder_arr = array($placeholder => WifiChartsConstants::DATA_NULL_VALUE);
      }
      $content[$key] = markup_content($value, $placeholder_arr);
    }
    elseif ($key === WifiChartsConstants::KEY_LAST_UPDATED) {
      $content[$key] = date_last_updated_content(WifiChartsConstants::DATA_NULL_LATEST_DATE, '', '<br />');
    }
  }

  return $content;
}
