<?php

/**
 * @file
 * This file contains commonly used constants as well as
 * a function to connect to an external DB.
 */

/**
 *
 */
class WifiChartsConstants {

  // The key used to connect to the external database.
  const WIFI_CHARTS_KEY = 'uw_wifi_charts';

  const B_ID_UW = '1';

  // @const
  /**
   * Buildings in the DB that aren't actually buildings.
   */
  private static $NOT_BUILDINGS_ARR = array(
    'UW',
    'Top',
    'DEV equipment',
    'Air Monitors',
    'Access Points',
    'Controllers',
  );

  // @const
  /**
   * The WiFi types that operate 2.4GHz.
   */
  private static $FREQ_2_4GHZ = array('b', 'g', 'n');
  // @const
  /**
   * The Wifi types that operate on 5GHz.
   */
  private static $FREQ_5GHZ = array('a', 'N', 'ac');
  // @const
  /**
   * The WiFi types that operate on 60GHz.
   */
  private static $FREQ_60GHZ = array('ad');
  // @const
  /**
   * The characters used by drupal in the t() function to indicate placeholders.
   */
  private static $DRUPAL_T_PLACEHOLDERS = array('@', '!', '%');

  // Datarate conversions.
  const KILOBIT = 1024;
  const MEGABIT = 1048576;
  const GIGABIT = 1073741824;
  const BPS = 'b/s';
  const KBPS = 'kb/s';
  const MBPS = 'Mb/s';
  const GBPS = 'Gb/s';

  // CSS files.
  const CSS_TABLE_ALIGN_RIGHT = '/includes/styles/uw_wifi_charts.table_align_right.css';
  const CSS_NO_LINK_DECOR = '/includes/styles/uw_wifi_charts.no_link_decor.css';

  // Configuration variable names used by Drupal.
  const CONFIG_SHOW_DATA_DAYS_COVER = 'uw_wifi_charts_num_days_to_display_data_cover_page';
  const CONFIG_SHOW_DATA_DAYS = 'uw_wifi_charts_num_days_to_display_data';
  const CONFIG_ROGUES_ENABLED = 'uw_wifi_charts_rogues_enabled';
  // DB Setting Default values.
  const CONFIG_DEFAULT_SHOW_DATA_DAYS_COVER = 1;
  const CONFIG_DEFAULT_SHOW_DATA_DAYS = 7;
  const CONFIG_DEFAULT_ROGUES_ENABLED = FALSE;
  // DB data pull interval in seconds.
  const DB_DATA_PULL_INTERVAL = 900;
  // Message for creating/deleting config variables at install/uninstall.
  const CONFIG_VARIABLES_CREATED_MSG = 'UWaterloo WiFi Charts configuration variables have been created.';
  const CONFIG_VARIABLES_DELETED_MSG = 'UWaterloo WiFi Charts configuration variables have been deleted.';

  // WiFi Charts module permissions.
  const PERMISSION_ADMINISTER_WIFI_CHARTS = 'administer uwaterloo wifi charts';
  const PERMISSION_TITLE_ADMINISTER_WIFI_CHARTS = 'Administer UWaterloo WiFi Charts';
  const PERMISSION_DESC_ADMINISTER_WIFI_CHARTS = 'Perform administrative tasks on UWaterloo WiFi Charts functionality';

  // Paths to URLs.
  const LINK_SETTINGS_GROUP  = 'admin/config/uw-wifi-charts';
  const LINK_SETTINGS_PAGE   = 'admin/config/uw-wifi-charts/manage';
  const LINK_COVER_PAGE      = 'statistics/wifi-charts';
  const LINK_CHARTS_BUILDING = 'statistics/wifi-charts/building/%s';
  const LINK_TABLES_BUILDING = 'statistics/wifi-charts/building/%s/data';
  const LINK_BUILDING_INDEX  = 'statistics/wifi-charts/building-select-index';
  const LINK_ROGUE_APS       = 'statistics/wifi-charts/building/1/rogues';
  const LINK_ROGUE_TABLE     = 'statistics/wifi-charts/building/1/rogues/data';

  // Link to URLs
  const LINK_CORE_LINKS  = 'https://istns.uwaterloo.ca/netstats';
  const LINK_WIRED_NETWORK  = 'http://istns.uwaterloo.ca/uwna/index.php?service=WIRED';
  const LINK_WIRELESS_NETWORK  = 'http://istns.uwaterloo.ca/uwna/index.php?service=WIRELESS';

  // Labels for navigating between pages.
  const VIEW_CHARTS = 'View charts of this data';
  const VIEW_TABLES = 'View raw data for these charts';
  const GO_TO_ROGUE_CHARTS = 'View charts of rogue APs';
  const VIEW_CHARTS_ROGUE = 'View chart of this data';
  const VIEW_TABLES_ROGUE = 'View raw data for this chart';
  const VIEW_BUILDING_INDEX = 'View buildings as table';
  const ERR_RETRIEVE_DATA = 'Could not retrieve % data. Please try again later.';

  // Labels for settings page.
  const SETTINGS_OVERVIEW_DESCRIPTION = 'This interface allows administrators to manage general uWaterloo WiFi Charts Settings.';
  const SETTINGS_DISPLAY_DB_TITLE = 'Display and Database Settings';
  const SETTINGS_ENABLE_ROGUE_CHART = 'Enable Rogue Chart';
  const SETTINGS_ENABLE_ROGUE_CHART_DESC = 'When enabled, the WiFi Charts pages will display charts indicating the number of rogues and suspected rogues.';
  const SETTINGS_NUM_DAYS_KEEP_DATA = 'Delete WiFi Charts data older than';
  const SETTINGS_NUM_DAYS_KEEP_DATA_DESC = 'At every cron run, WiFi Charts module will check for and delete data that is older than the user-specified time in days. <br /> Set to 0 to always keep data.';
  const SETTINGS_NUM_DAYS_DISPLAY_DATA = 'When to start displaying data';
  const SETTINGS_NUM_DAYS_DISPLAY_DATA_DESC = 'e.g. Set to 7 to start displaying data from charts starting 7 days back. This applies to clients over time, network usage over time, connection mode over time, and rogue AP over time charts.';
  const SETTINGS_NUM_DAYS_DISPLAY_DATA_COVER = 'When to start displaying data (Cover Page)';
  const SETTINGS_NUM_DAYS_DISPLAY_DATA_COVER_DESC = 'e.g. Set to 7 to start displaying data from charts starting 7 days back for the cover page.';
  const SETTINGS_BUILDINGS_MISSING_NAMES_TITLE = 'Buildings With Missing Names';
  const SETTINGS_BUILDINGS_MISSING_NAMES_B_CODE = 'Building code: ';
  const SETTINGS_BUILDINGS_MISSING_NAMES_B_CODE_DESC = 'Building code from AirWave.';
  const SETTINGS_BUILDINGS_MISSING_NAMES_B_NAME = 'Building name';
  const SETTINGS_BUILDINGS_MISSING_NAMES_B_NAME_DESC = '(alphanumeric characters, spaces, hyphens, and single quotes are permitted.)';
  const SETTINGS_BUILDINGS_MISSING_LOC_TITLE = 'Buildings With Missing Location Data';
  const SETTINGS_BUILDINGS_MISSING_LOC_B_CODE = 'Building Code: ';
  const SETTINGS_BUILDINGS_MISSING_LOC_B_CODE_DESC = 'Building code from AirWave.';
  const SETTINGS_BUILDINGS_MISSING_LOC_LATITUDE = 'Latitude';
  const SETTINGS_BUILDINGS_MISSING_LOC_LATITUDE_DESC = 'Latitude (Precision: 5 - 8 decimals)';
  const SETTINGS_BUILDINGS_MISSING_LOC_LONGITUDE = 'Longitude';
  const SETTINGS_BUILDINGS_MISSING_LOC_LONGITUDE_DESC = 'Longitude (Precision: 5 - 8 decimals)';
  const SETTINGS_SAVE_CONFIGURATION = 'Save Configuration';
  const SETTINGS_SAVE_CONFIGURATION_DESC = 'Save the settings to the UW WiFi Charts DB.';
  const SETTINGS_INVALID_NUM_DAYS_NON_NEGA_INT = 'Invalid number of days format. Must be a non-negative integer.';
  const SETTINGS_INVALID_NUM_DAYS_POS_INT = 'Invalid number of days format. Must be a positive integer.';
  const SETTINGS_INVALID_NUM_DAYS_LESS_THAN_KEEP_DATA = 'Invalid number of days. Number of days of data to be displayed must be less than or equal to the number of days to keep data.';
  const SETTINGS_INVALID_BUILDING_NAME = 'Invalid building name. (Valid characters: a-z, A-Z, 0-9, space, hypen, and single quote)';
  const SETTINGS_INVALID_LATITUDE = 'Invalid latitude. Must be a number with 5 - 8 decimals between -90 and 90.)';
  const SETTINGS_INVALID_LONGITUDE = 'Invalid longitude. Must be a number with 5 - 8 decimals between -180 and 180.)';
  const SETTINGS_SUBMIT_SUCCESSFUL = 'The configuration options have been saved.';
  const SETTINGS_SUBMIT_FAILED = 'An error occured. Settings were not saved.';

  // Labels for building selection.
  const BUILDING_INDEX_TITLE = 'List of buildings';
  const BUILDING_INDEX_TABLE_ID = 'building-index-table';
  const BUILDING_INDEX_BUILDING_URL = 'statistics/wifi-charts/building/';
  const BUILDING_INDEX_HEADER_B_NAME = 'Building name';
  const BUILDING_INDEX_HEADER_B_NAME_ABBR = 'Building';
  const BUILDING_INDEX_HEADER_LOC = 'Location (lat, long)';
  const BUILDING_INDEX_HEADER_LOC_ABBR = 'Location';
  const BUILDING_INDEX_HEADER_CLIENTS = 'Current client count';
  const BUILDING_INDEX_HEADER_CLIENTS_ABBR = 'Clients';
  const BUILDING_INDEX_HEADER_DOWNLOAD = 'Current total download rate';
  const BUILDING_INDEX_HEADER_DOWNLOAD_ABBR = 'Download';
  const BUILDING_INDEX_HEADER_UPLOAD = 'Current total upload rate';
  const BUILDING_INDEX_HEADER_UPLOAD_ABBR = 'Upload';

  const BUILDING_LOC_UNAVAILABLE = 'Unavailable';

  // Labels and assoc array keys for building charts and tables.
  // The name of the keys of the markup for labels will always end in this suffix.
  const KEY_CURRENT_DATA_SUFFIX = '_current';
  const KEY_LAST_UPDATED = 'last_updated';
  const KEY_MENU_LINKS = 'menu_links';
  const KEY_ANCHOR_LINKS_PREFIX = 'ref_anchor_';
  // Used when no data is available. e.g. Current client count: --.
  const DATA_NULL_VALUE = '--';
  const DATA_NULL_LATEST_DATE = 'N/A';
  const LABEL_NO_JS = 'Please enable JavaScript to view charts.';
  const LABEL_NAV_MENU = 'Menu: ';

  // Static page title labels.
  const LABEL_TITLE_COVER_PAGE = 'WiFi charts';
  const LABEL_DESC_COVER_PAGE = 'UWaterloo wifi data for the past 24 hours';
  const LABEL_TITLE_CHARTS_UW = 'UWaterloo campus data';
  const LABEL_DESC_CHARTS_UW = 'Total network data of the University of Waterloo Campus';
  const LABEL_TITLE_TABLES_UW = 'UWaterloo campus data tables';
  const LABEL_DESC_TABLES_UW = 'Total network data of the University of Waterloo Campus in table format';
  const LABEL_TITLE_CHARTS_ROGUE_APS = 'Rogue access points';
  const LABEL_DESC_CHARTS_ROGUE_APS = 'Number of rogue access points chart';
  const LABEL_TITLE_TABLES_ROGUE_APS = 'Rogue access points table';
  const LABEL_DESC_TABLES_ROGUE_APS = 'Number of rogue access points data table';
  const LABEL_TITLE_BUILDING_INDEX = 'Index of buildings';
  const LABEL_DESC_BUILDING_INDEX = 'View each building\'s data in a table';
  const LABEL_TITLE_SETTINGS_GROUP = 'UWaterloo WiFi Charts';
  const LABEL_DESC_SETTINGS_GROUP = 'Administer UWaterloo Wifi Charts settings';
  const LABEL_TITLE_SETTINGS_PAGE = 'UWaterloo WiFi Charts settings';
  const LABEL_DESC_SETTINGS_PAGE = 'Configure UWaterloo WiFi Charts settings.';

  // Static link labels
  const LABEL_LINK_ABOVE_DESC = 'For real-time information on network uptime and performance, please see the following links:';
  const LABEL_LINK_CORE_LINKS = 'Core links stats';
  const LABEL_LINK_WIRED_NETWORK = 'Wired network stats';
  const LABEL_LINK_WIRELESS_NETWORK = 'Wireless network stats';
  const LABEL_LINK_WIFI_CHARTS = 'Wi-Fi charts';

  // About labels.
  const LABEL_ABOUT_CHARTS = 'Client and network bandwidth data is updated every 15 minutes, while device OS and connection mode breakdowns and failed login attempts are updated daily, displaying the total data for the day before, for a period of 24 hours. ';
  const LABEL_ABOUT_ROGUES = 'Rogue access point data is updated every 15 minutes.';

  // Dyanmic labels.
  const LABEL_NO_PAGE_TITLE_PREFIX = 'Building @b_id @suffix';
  const LABEL_PAGE_TITLE_SUFFIX_AGGREGATE_TOTAL = '@b_name Total @suffix';
  const LABEL_PAGE_TITLE_SUFFIX_AGGREGATE_NUM = '@b_name 1 - @max @suffix';
  const LABEL_NO_DATA_CHART_TITLE = '@title_text Data';
  const LABEL_NO_DATA_CHART = 'No data available for @chart_type data. Please try again later.';
  const LABEL_NUM_CLIENTS = 'Number of clients: @client_count';
  const LABEL_CURR_DOWNLOAD_SPEED = 'Current total download speed: !down_speed';
  const LABEL_CURR_UPLOAD_SPEED = 'Current total upload speed: !up_speed';
  const LABEL_FAILED_LOGINS = 'Number of failed logins yesterday: @failed_login_count';
  const LABEL_NUM_ROGUE_AP = 'Number of rogue access points: @rogue_ap_count';
  const LABEL_LAST_UPDATED = '(Last updated:  @curr_datetime)';
  // Placeholders for the above labels.
  const LABEL_PLACEHOLDER_NO_PAGE_TITLE_PREFIX_B_ID = '@b_id';
  const LABEL_PLACEHOLDER_NO_PAGE_TITLE_PREFIX_SUFFIX = '@suffix';
  const LABEL_PLACEHOLDER_PAGE_TITLE_SUFFIX_AGGREGATE_B_NAME = '@b_name';
  const LABEL_PLACEHOLDER_PAGE_TITLE_SUFFIX_AGGREGATE_SUFFIX = '@suffix';
  const LABEL_PLACEHOLDER_PAGE_TITLE_SUFFIX_AGGREGATE_MAX = '@max';
  const LABEL_PLACEHOLDER_NO_DATA_CHART_TITLE = '@title_text';
  const LABEL_PLACEHOLDER_NO_DATA_CHART = '@chart_type';
  const LABEL_PLACEHOLDER_NUM_CLIENTS = '@client_count';
  const LABEL_PLACEHOLDER_CURR_DOWNLOAD_SPEED = '!down_speed';
  const LABEL_PLACEHOLDER_CURR_UPLOAD_SPEED = '!up_speed';
  const LABEL_PLACEHOLDER_FAILED_LOGINS = '@failed_login_count';
  const LABEL_PLACEHOLDER_NUM_ROGUE_AP = '@rogue_ap_count';
  const LABEL_PLACEHOLDER_LAST_UPDATED = '@curr_datetime';

  // Miscellaneous.
  const ANCHOR_JUMP_TO_LABEL = 'Jump to:';
  const ANCHOR_LIST = 'list';
  const BACK_TO_TOP = 'Back to top';
  const CLASS_BACK_TO_TOP = 'back-to-top';
  const ERROR_RETRIEVING_DATA = 'Unable to retrieve data. Please try again later.';
  const CHART_ARIA_DESC = self::BUILDING_INDEX_BUILDING_URL;

  // Aggregate building codes with these prefixes:
  const PREFIX_V1_BLOCKS = 'V1 ';
  const PREFIX_CLV_SOUTH_BLOCKS = 'CLV South Block ';

  // Chart default atttributes.
  const CHART_LINE_HEIGHT = 400;
  const CHART_LINE_WIDTH = 750;
  const CHART_LINE_LABEL_FONT_SIZE = 10.5;
  const CHART_PIE_HEIGHT = 400;
  const CHART_PIE_WIDTH = 600;

  // Clients over time chart definitions.
  const CHART_CLIENTS_TIME_TITLE = 'Current # of clients over time';
  const CHART_CLIENTS_TIME_DATA_TYPE = 'client';
  const CHART_CLIENTS_TIME_ARIA_LABEL = 'Chart displaying current clients over time.';
  const CHART_CLIENTS_TIME_ANCHOR_LABEL = 'Clients over time';
  const CHART_CLIENTS_TIME_ANCHOR_ID = 'clients-chart';
  const CHART_CLIENTS_TIME_X_AXIS = 'Time';
  const CHART_CLIENTS_TIME_Y_AXIS_CLIENTS = 'Clients';

  // Usage over time chart definitions.
  const CHART_USAGE_TIME_TITLE = 'Current network usage (in @data_rate) over time';
  const CHART_USAGE_TIME_TITLE_PLACEHOLDER = '@data_rate';
  const CHART_USAGE_TIME_DATA_TYPE = 'usage';
  const CHART_USAGE_TIME_ARIA_LABEL = 'Chart displaying current network usage over time.';
  const CHART_USAGE_TIME_ANCHOR_LABEL = 'Network usage over time';
  const CHART_USAGE_TIME_ANCHOR_ID = 'network-usage-chart';
  const CHART_USAGE_TIME_X_AXIS = 'Time';
  const CHART_USAGE_TIME_Y_AXIS_DOWNLOAD = 'Download';
  const CHART_USAGE_TIME_Y_AXIS_UPLOAD = 'Upload';

  // Device OS breakdown definitions.
  const CHART_DEVICE_OS_PIE_TITLE = 'Device OS 24h breakdown';
  const CHART_DEVICE_OS_PIE_DATA_TYPE = 'device OS';
  const CHART_DEVICE_OS_PIE_ARIA_LABEL = 'A chart displaying the number of devices on the network of each operating system.';
  const CHART_DEVICE_OS_PIE_ANCHOR_LABEL = 'Device OS breakdown';
  const CHART_DEVICE_OS_PIE_ANCHOR_ID = 'device-os-pie-chart';

  // Connection mode breakdown definitions.
  const CHART_CONNECTION_MODE_PIE_TITLE = 'Connection mode 24h breakdown';
  const CHART_CONNECITON_MODE_PIE_DATA_TYPE = 'conneciton mode';
  const CHART_CONNECTION_MODE_PIE_ARIA_LABEL = 'A chart of the number of devices using 2.4GhZ or 5Ghz WiFi bands.';
  const CHART_CONNECTION_MODE_PIE_ANCHOR_LABEL = 'Connection mode breakdown';
  const CHART_CONNECTION_MODE_PIE_ANCHOR_ID = 'connection-mode-pie-chart';
  const CHART_CONNECTION_MODE_2_4GHZ = '2.4GHz';
  const CHART_CONNECTION_MODE_5GHZ = '5GHz';
  const CHART_CONNECTION_MODE_60GHZ = '60GHz';
  const CHART_CONNECTION_MODE_OTHER_GHZ = 'Unknown';

  // Device OS over time definitions.
  const CHART_DEVICE_OS_TIME_TITLE = 'Device OS over time (24h Totals)';
  const CHART_DEVICE_OS_TIME_ARIA_LABEL = 'This line chart displays the number of clients using each device OS over time.';
  const CHART_DEVICE_OS_TIME_ANCHOR_LABEL = 'Clients per device OS over time';
  const CHART_DEVICE_OS_TIME_ANCHOR_ID = 'device-os-line-chart';
  const CHART_DEVICE_OS_TIME_X_AXIS = 'Date';

  // Connection mode over time definitions.
  const CHART_CONNECTION_MODE_TIME_TITLE = '% Clients per connection mode over time';
  const CHART_CONNECITON_MODE_TIME_DATA_TYPE = 'connection mode over time';
  const CHART_CONNECTION_MODE_TIME_ARIA_LABEL = 'This line chart displays the percentage of clients using the 2.4GHz or 5GHz band over time.';
  const CHART_CONNECTION_MODE_TIME_ANCHOR_LABEL = '% of clients per connection mode over time';
  const CHART_CONNECTION_MODE_TIME_ANCHOR_ID = 'connection-mode-line-chart';
  const CHART_CONNECTION_MODE_TIME_X_AXIS = 'Date';
  const CHART_CONNECTION_MODE_TIME_Y_AXIS_SUFFIX = ' Clients';

  // RADIUS Authentication Issue chart definitions.
  const CHART_FAILED_LOGINS_TIME_TITLE = 'Total failed logins per day';
  const CHART_FAILED_LOGINS_TIME_DATA_TYPE = 'failed logins';
  const CHART_FAILED_LOGINS_TIME_ARIA_LABEL = 'Chart displaying daily total failed login attempts.';
  const CHART_FAILED_LOGINS_ANCHOR_LABEL = 'Failed logins per day';
  const CHART_FAILED_LOGINS_ANCHOR_ID = 'auth-issues-chart';
  const CHART_FAILED_LOGINS_X_AXIS = 'Time';
  const CHART_FAILED_LOGINS_Y_AXIS_FAILED_LOGINS = 'Failed logins';

  // Rogue AP chart definitions.
  const CHART_ROGUE_AP_TIME_TITLE = 'Number of rogue access points detected';
  const CHART_ROGUE_AP_TIME_DATA_TYPE = 'rogue';
  const CHART_ROGUE_AP_TIME_ARIA_LABEL = 'Chart displaying total current detected rogue access points over time.';
  const CHART_ROGUE_AP_TIME_ANCHOR_LABEL = 'Rogue APs over time';
  const CHART_ROGUE_AP_TIME_ANCHOR_ID = 'rogues-chart';
  const CHART_ROGUE_AP_TIME_X_AXIS = 'Time';
  const CHART_ROGUE_AP_TIME_Y_AXIS_ROGUES = 'Rogue APs';

  // Labels for building tables.
  const TABLE_NO_DATA_TEXT = 'Could not retrieve data.';
  const TABLE_DEFAULT_CLASS = 'uw_wifi_charts_table';

  const TABLE_NETWORK_DATA_TITLE = 'Network data';
  const TABLE_NETWORK_DATA_ANCHOR_LABEL = 'Network data';
  const TABLE_NETWORK_DATA_TABLE_ID = 'network-data-table';
  const TABLE_NETWORK_DATA_HEADER_TIME = 'Time';
  const TABLE_NETWORK_DATA_HEADER_TIME_ABBR = 'Time';
  const TABLE_NETWORK_DATA_HEADER_CLIENTS = 'Client count';
  const TABLE_NETWORK_DATA_HEADER_CLIENTS_ABBR = 'Clients';
  const TABLE_NETWORK_DATA_HEADER_DOWNLOAD = 'Download speed';
  const TABLE_NETWORK_DATA_HEADER_DOWNLOAD_ABBR = 'Download';
  const TABLE_NETWORK_DATA_HEADER_UPLOAD = 'Upload speed';
  const TABLE_NETWORK_DATA_HEADER_UPLOAD_ABBR = 'Upload';

  const TABLE_DEVICE_OS_BREAKDOWN_TITLE = 'Device OS';
  const TABLE_DEVICE_OS_BREAKDOWN_ANCHOR_LABEL = 'Device OS breakdown';
  const TABLE_DEVICE_OS_BREAKDOWN_TABLE_ID = 'device-os-table';
  const TABLE_DEVICE_OS_BREAKDOWN_HEADER_OS = 'OS';
  const TABLE_DEVICE_OS_BREAKDOWN_HEADER_OS_ABBR = 'OS';
  const TABLE_DEVICE_OS_BREAKDOWN_HEADER_CLIENTS = 'Number of devices';
  const TABLE_DEVICE_OS_BREAKDOWN_HEADER_CLIENTS_ABBR = 'Devices';

  const TABLE_CONNECTION_MODE_BREAKDOWN_TITLE = 'Connection mode';
  const TABLE_CONNECTION_MODE_BREAKDOWN_ANCHOR_LABEL = 'Connection mode breakdown';
  const TABLE_CONNECTION_MODE_BREAKDOWN_TABLE_ID = 'connection-mode-table';
  const TABLE_CONNECTION_MODE_BREAKDOWN_HEADER_MODE = 'Connection mode';
  const TABLE_CONNECTION_MODE_BREAKDOWN_HEADER_MODE_ABBR = 'Band';
  const TABLE_CONNECTION_MODE_BREAKDOWN_HEADER_CLIENTS = 'Number of devices';
  const TABLE_CONNECTION_MODE_BREAKDOWN_HEADER_CLIENTS_ABBR = 'Devices';

  const TABLE_DEVICE_OS_TIME_ANCHOR_LABEL = 'Clients per device OS over time';
  const TABLE_DEVICE_OS_TIME_TABLE_ID = 'device-os-time-table';

  const TABLE_CONNECTION_MODE_TIME_TITLE = 'Connection mode over time';
  const TABLE_CONNECTION_MODE_TIME_ANCHOR_LABEL = 'Connection mode over time';
  const TABLE_CONNECTION_MODE_TIME_TABLE_ID = 'connection-mode-time-table';
  const TABLE_CONNECTION_MODE_TIME_HEADER_DATE = 'Date';
  const TABLE_CONNECTION_MODE_TIME_HEADER_DATE_ABBR = 'Date';
  const TABLE_CONNECTION_MODE_TIME_HEADER_MODE = 'Connection mode';
  const TABLE_CONNECTION_MODE_TIME_HEADER_MODE_ABBR = 'Band';
  const TABLE_CONNECTION_MODE_TIME_HEADER_CLIENTS = 'Number of devices';
  const TABLE_CONNECTION_MODE_TIME_HEADER_CLIENTS_ABBR = 'Devices';
  const TABLE_CONNECTION_MODE_TIME_HEADER_PCT = '% of devices';
  const TABLE_CONNECTION_MODE_TIME_HEADER_PCT_ABBR = 'Device %';

  const TABLE_RADIUS_AUTH_ISSUES_TITLE = 'Failed authentication attempts';
  const TABLE_RADIUS_AUTH_ISSUES_ANCHOR_LABEL = 'Failed logins per day';
  const TABLE_RADIUS_AUTH_ISSUES_TABLE_ID = 'auth-issues-table';
  const TABLE_RADIUS_AUTH_ISSUES_HEADER_DATE = 'Date';
  const TABLE_RADIUS_AUTH_ISSUES_HEADER_DATE_ABBR = 'Date';
  const TABLE_RADIUS_AUTH_ISSUES_HEADER_FAILED_LOGINS = 'Number of failed login attempts';
  const TABLE_RADIUS_AUTH_ISSUES_HEADER_FAILED_LOGINS_ABBR = 'Failed logins';

  const TABLE_ROGUES_TITLE = 'Detected rogue access points';
  const TABLE_ROGUES_TABLE_ID = 'rogues-table';
  const TABLE_ROGUES_HEADER_TIME = 'Time';
  const TABLE_ROGUES_HEADER_TIME_ABBR = 'Time';
  const TABLE_ROGUES_HEADER_ROGUES = 'Total rogue APs';
  const TABLE_ROGUES_HEADER_ROGUES_ABBR = 'Rogues';

  /**
   *
   */
  public static function getNotBuildings() {
    return self::$NOT_BUILDINGS_ARR;
  }

  /**
   *
   */
  public static function get2_4GhzWifiTypes() {
    return self::$FREQ_2_4GHZ;
  }

  /**
   *
   */
  public static function get5GhzWifiTypes() {
    return self::$FREQ_5GHZ;
  }

  /**
   *
   */
  public static function get60GhzWifiTypes() {
    return self::$FREQ_60GHZ;
  }

  /**
   *
   */
  public static function getDrupalTPlaceholders() {
    return self::$DRUPAL_T_PLACEHOLDERS;
  }

}
