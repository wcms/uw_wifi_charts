<?php

/**
 * @file
 * Page callback functions for hook_menu()
 * Parses data from wifi_charts.page_queries.inc so that they can be populated
 * into charts and tables.
 */

require_once __DIR__ . '/uw_wifi_charts.page_queries.inc';
require_once __DIR__ . '/uw_wifi_charts.constants.inc';

/**
 * Page callback function for the WiFi Charts main page.
 * This is the cover page.
 *
 * @see hook_menu()
 */
function uw_wifi_charts_main() {
  $building_id = WifiChartsConstants::B_ID_UW;
  // Get number of days of data to show for the cover page..
  $show_data_days = variable_get(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS_COVER, WifiChartsConstants::CONFIG_DEFAULT_SHOW_DATA_DAYS_COVER);

  // Get renderable arrays for network data. (clients, download/upload speeds)
  $building_data = network_data($building_id, WifiChartsConstants::CHART_CLIENTS_TIME_ANCHOR_ID, WifiChartsConstants::CHART_USAGE_TIME_ANCHOR_ID, $show_data_days, TRUE);

  // Create list of anchors with the name and anchor id.
  $charts = array(
    'clients_over_time' => array(
      'name'      => WifiChartsConstants::CHART_CLIENTS_TIME_ANCHOR_LABEL,
      'id'        => WifiChartsConstants::CHART_CLIENTS_TIME_ANCHOR_ID,
    ),
    'network_usage_over_time' => array(
      'name'      => WifiChartsConstants::CHART_USAGE_TIME_ANCHOR_LABEL,
      'id'        => WifiChartsConstants::CHART_USAGE_TIME_ANCHOR_ID,
    ),
  );

  // Create Content for webpage.
  $content = array();

  // Create the top level menu links.
  $content[WifiChartsConstants::KEY_MENU_LINKS] = top_navigation_links();

  // If data retrieval unsuccesful, display error markup.
  if ($building_data === FALSE) {
    $content['info'] = array(
      '#type'   => 'markup',
      '#markup' => WifiChartsConstants::ERROR_RETRIEVING_DATA,
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    );
  }

  // Get current data markup.
  $content['clients' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX]  = $building_data['clients' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX];
  $content['download' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX] = $building_data['download' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX];
  $content['upload' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX]   = $building_data['upload' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX];
  $content[WifiChartsConstants::KEY_LAST_UPDATED]                     = $building_data[WifiChartsConstants::KEY_LAST_UPDATED];

  // Create list of URL Anchors.
  $content['anchor_link_list'] = anchor_link_list($charts);

  // Link to accessible page with tables.
  $content['link_to_tables'] = create_hyperlink_content(sprintf(WifiChartsConstants::LINK_TABLES_BUILDING, $building_id), WifiChartsConstants::VIEW_TABLES);

  // Markup to show when JS is disabled.
  $content['no_js'] = no_js_markup();

  // Get chart arrays and "back to top" links.
  // Chart for clients over time for the past X days.
  $content['clients_over_time'] = $building_data['clients_over_time_chart'];

  // Back to top anchor, jumps to the list of anchor links.
  $content['back_to_top_clients'] = ref_anchor(WifiChartsConstants::ANCHOR_LIST, WifiChartsConstants::BACK_TO_TOP, '<p>', '</p>', WifiChartsConstants::CLASS_BACK_TO_TOP);

  // Chart for network throughput over time for the past X days.
  $content['usage_over_time'] = $building_data['usage_over_time_chart'];

  // Back to top anchor, jumps to the list of anchor links.
  $content['back_to_top_usage'] = ref_anchor(WifiChartsConstants::ANCHOR_LIST, WifiChartsConstants::BACK_TO_TOP, '<p>', '</p>', WifiChartsConstants::CLASS_BACK_TO_TOP);

  // $content['connection_mode_over_time'] = connection_mode_line_chart($building_id, 'connection-mode-over-time', $show_data_days, TRUE);
  // Some information on how often data is updated here.
  $content['about_info'] = about_label();

  return $content;
}

/**
 * Title Callback for wifi-charts/%.
 *
 * @see hook_menu()
 */
function get_building_title($building_id, $append_at_end_of_title = '') {
  // Given the building_id, get a list of building_ids that should also included
  // in the result set. (For CLV South blocks, V1 N/S/W/E blocks, etc.)
  $building_ids = get_building_ids_to_aggregate($building_id);

  db_set_active(WifiChartsConstants::WIFI_CHARTS_KEY);

  try {
    // Look up BUILDING_CODE based on BUILDING_ID.
    $building_name_query = db_query(
      'SELECT BUILDING_ID, BUILDING_NAME, BUILDING_CODE ' .
      'FROM {BUILDINGS} ' .
      'WHERE BUILDING_ID IN (:building_ids) ' .
      'ORDER BY BUILDING_ID ASC',
      array(
        ':building_ids' => $building_ids,
      ));
  }
  catch (Exception $e) {
    db_set_active();
    return t(WifiChartsConstants::LABEL_NO_PAGE_TITLE_PREFIX,
      array(
        WifiChartsConstants::LABEL_PLACEHOLDER_NO_PAGE_TITLE_PREFIX_B_ID => $building_id,
        WifiChartsConstants::LABEL_PLACEHOLDER_NO_PAGE_TITLE_PREFIX_SUFFIX => $append_at_end_of_title,
      ));
  }

  // Check if result query is empty.
  if (!$building_name_query->rowCount()) {
    // Building does not exist.
    display_404();
  }
  // User-specified $building_id exists in the DB.
  else {
    // Get building code and building name of first row.
    $building_row = $building_name_query->fetchObject();
    $building_name = $building_row->BUILDING_NAME;
    $building_code = $building_row->BUILDING_CODE;
    // Remove the number/letter at the end of the building.
    $building_name_partial = explode(' ', $building_name, -1);
    $building_name_partial = implode(' ', $building_name_partial);
    // V1 and CLV Houses are aggregated so adjust the title to represent this.
    if (strpos($building_code, WifiChartsConstants::PREFIX_CLV_SOUTH_BLOCKS) !== FALSE) {
      // Aggregate all CLV South Blocks, append Total at the end.
      $building_name = t(WifiChartsConstants::LABEL_PAGE_TITLE_SUFFIX_AGGREGATE_TOTAL,
        array(
          WifiChartsConstants::LABEL_PLACEHOLDER_PAGE_TITLE_SUFFIX_AGGREGATE_B_NAME => $building_name_partial,
          WifiChartsConstants::LABEL_PLACEHOLDER_PAGE_TITLE_SUFFIX_AGGREGATE_SUFFIX => $append_at_end_of_title,
        ));
    }
    elseif (strpos($building_code, WifiChartsConstants::PREFIX_V1_BLOCKS) !== FALSE) {
      // Aggregate by North/South/East/West by removing the number at the end and indicate which buildings were aggregated.
      $building_name = t(WifiChartsConstants::LABEL_PAGE_TITLE_SUFFIX_AGGREGATE_NUM,
        array(
          WifiChartsConstants::LABEL_PLACEHOLDER_PAGE_TITLE_SUFFIX_AGGREGATE_B_NAME => $building_name_partial,
          WifiChartsConstants::LABEL_PLACEHOLDER_PAGE_TITLE_SUFFIX_AGGREGATE_MAX    => $building_name_query->rowCount(),
          WifiChartsConstants::LABEL_PLACEHOLDER_PAGE_TITLE_SUFFIX_AGGREGATE_SUFFIX => $append_at_end_of_title,
        ));
    }
  }
  db_set_active();
  return $building_name;
}

/**
 * Page Callback for wifi-charts/building/%.
 *
 * @see hook_menu()
 */
function uw_wifi_charts_building($building_id) {
  if ((int) $building_id <= 0 || !is_numeric($building_id)) {
    display_404();
  }

  // Get WiFi Charts settings.
  $show_rogues = variable_get(WifiChartsConstants::CONFIG_ROGUES_ENABLED,
  // Rogue AP's chart enabled?
    WifiChartsConstants::CONFIG_DEFAULT_ROGUES_ENABLED);
  $show_data_days = variable_get(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS,
  // How many days to show data in the charts.
    WifiChartsConstants::CONFIG_DEFAULT_SHOW_DATA_DAYS);

  // List the anchor labels and ids here.
  $charts = array(
    'clients_over_time' => array(
      'name' => WifiChartsConstants::CHART_CLIENTS_TIME_ANCHOR_LABEL,
      'id'   => WifiChartsConstants::CHART_CLIENTS_TIME_ANCHOR_ID,
    ),
    'network_usage_over_time' => array(
      'name' => WifiChartsConstants::CHART_USAGE_TIME_ANCHOR_LABEL,
      'id'   => WifiChartsConstants::CHART_USAGE_TIME_ANCHOR_ID,
    ),
    'device_os_breakdown' => array(
      'name' => WifiChartsConstants::CHART_DEVICE_OS_PIE_ANCHOR_LABEL,
      'id'   => WifiChartsConstants::CHART_DEVICE_OS_PIE_ANCHOR_ID,
    ),
    'connection_mode_breakdown' => array(
      'name' => WifiChartsConstants::CHART_CONNECTION_MODE_PIE_ANCHOR_LABEL,
      'id'   => WifiChartsConstants::CHART_CONNECTION_MODE_PIE_ANCHOR_ID,
    ),
    'connection_mode_over_time' => array(
      'name' => WifiChartsConstants::CHART_CONNECTION_MODE_TIME_ANCHOR_LABEL,
      'id'   => WifiChartsConstants::CHART_CONNECTION_MODE_TIME_ANCHOR_ID,
    ),
  );
  if ($building_id === WifiChartsConstants::B_ID_UW) {
    $charts['auth_issues_over_time'] = array(
      'name'      => WifiChartsConstants::CHART_FAILED_LOGINS_ANCHOR_LABEL,
      'id'        => WifiChartsConstants::CHART_FAILED_LOGINS_ANCHOR_ID,
    );
  }

  // Get all markup for network data (clients over time, network throughput over time).
  $building_data = network_data($building_id, $charts['clients_over_time']['id'], $charts['network_usage_over_time']['id'], $show_data_days);

  if ($building_id === WifiChartsConstants::B_ID_UW) {
    // If on the campus total page, get the RADIUS authentication failure counts over time.
    $radius_auth_issues_content = radius_auth_chart(WifiChartsConstants::CHART_FAILED_LOGINS_ANCHOR_ID, $show_data_days, TRUE);
  }

  // Get charts of device OS breakdown and conection mode data.
  $device_os_chart_pie_markup = device_os_pie_chart($building_id, WifiChartsConstants::CHART_DEVICE_OS_PIE_ANCHOR_ID, TRUE);
  // $device_os_chart_line_markup = device_os_line_chart($building_id, 'device-os-line-chart', $show_data_days, TRUE);.
  $connection_mode_chart_pie_markup = connection_mode_pie_chart($building_id, WifiChartsConstants::CHART_CONNECTION_MODE_PIE_ANCHOR_ID, TRUE);
  $connection_mode_chart_line_markup = connection_mode_line_chart($building_id, WifiChartsConstants::CHART_CONNECTION_MODE_TIME_ANCHOR_ID, $show_data_days, TRUE);

  // Create Content for webpage.
  $content = array();

  // Get markup for top level menu links.
  $content[WifiChartsConstants::KEY_MENU_LINKS] = top_navigation_links();

  // Display list of current info.
  foreach ($building_data as $key => $value) {
    if (strpos($key, WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX) !== FALSE) {
      $content['current'][$key] = $value;
    }
  }
  if ($building_id === WifiChartsConstants::B_ID_UW) {
    // If looking at campus totals, display the failed logins chart.
    $content['current']['auth_issues' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX] = $radius_auth_issues_content['auth_issues' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX];
  }
  // Display markup for when the data was last updated.
  $content['current'][WifiChartsConstants::KEY_LAST_UPDATED] = $building_data[WifiChartsConstants::KEY_LAST_UPDATED];

  // Create list of URL Anchors.
  $content['anchor_links_list'] = anchor_link_list($charts);

  // If Rogues are enabled, display link to Rogues graph.
  if ($show_rogues && $building_id === WifiChartsConstants::B_ID_UW) {
    // Link to accessible page with tables.
    $content['links']['link_to_tables'] = create_hyperlink_content(sprintf(WifiChartsConstants::LINK_TABLES_BUILDING, $building_id), WifiChartsConstants::VIEW_TABLES);
    $content['links']['link_to_rogues'] = create_hyperlink_content(WifiChartsConstants::LINK_ROGUE_APS, WifiChartsConstants::GO_TO_ROGUE_CHARTS, '', '<br /> <br />');
  }
  else {
    // Link to accessible page with tables.
    $content['links']['link_to_tables'] = create_hyperlink_content(
      sprintf(WifiChartsConstants::LINK_TABLES_BUILDING, $building_id), WifiChartsConstants::VIEW_TABLES, '', '<br /><br />');
  }

  // Mkarup for when JS is disabled.
  $content['no_js'] = no_js_markup();

  // Initialize charts.
  foreach ($charts as $chart_name => $chart_info) {
    $content['charts'][$chart_name]['chart'] = array();
    $content['charts'][$chart_name]['back_to_top'] = ref_anchor(
      WifiChartsConstants::ANCHOR_LIST,
      WifiChartsConstants::BACK_TO_TOP,
      '<p>', '</p>',
      WifiChartsConstants::CLASS_BACK_TO_TOP
    );
  }

  // Display charts and back to top anchor links.
  // Clients over time chart.
  $content['charts']['clients_over_time']['chart'] = $building_data['clients_over_time_chart'];
  // Network throughput over time chart.
  $content['charts']['network_usage_over_time']['chart'] = $building_data['usage_over_time_chart'];

  // Device OS 24h breakdown pie chart.
  $content['charts']['device_os_breakdown']['chart'] = $device_os_chart_pie_markup['chart'];
  // Connection mode 24h breakdown pie chart.
  $content['charts']['connection_mode_breakdown']['chart'] = $connection_mode_chart_pie_markup['chart'];
  $content['charts']['last_updated_pie_charts']            = $connection_mode_chart_pie_markup[WifiChartsConstants::KEY_LAST_UPDATED];

  // % of clients per connection mode each day.
  $content['charts']['connection_mode_over_time']['chart'] = $connection_mode_chart_line_markup['chart'];
  // $content['device_os_over_time'] = $device_os_chart_line_markup['device_line_chart'];.
  if ($building_id === WifiChartsConstants::B_ID_UW) {
    // Daily number of failed logins for X days.
    $content['charts']['auth_issues_over_time']['chart'] = $radius_auth_issues_content['chart'];
  }
  // Information on when data is updated.
  $content['about_info'] = about_label();
  return $content;
}

/**
 * Page callback function for wifi-charts/building/%/data.
 *
 * @see hook_menu()
 * This page is built with accessibility in mind.
 */
function uw_wifi_charts_building_tables($building_id) {

  // Check if valid building.
  if ((int) $building_id <= 0 || !is_numeric($building_id)) {
    display_404();
  }

  // Get WiFi Charts settings.
  $show_rogues = variable_get(WifiChartsConstants::CONFIG_ROGUES_ENABLED,
  // Rogue AP's chart enabled?
    WifiChartsConstants::CONFIG_DEFAULT_ROGUES_ENABLED);
  $show_data_days = variable_get(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS,
  // How many days to show data in the charts.
    WifiChartsConstants::CONFIG_DEFAULT_SHOW_DATA_DAYS);

  // List of anchor names and ids of tables.
  $tables = array(
    'network_data_time' => array(
      'name' => WifiChartsConstants::TABLE_NETWORK_DATA_ANCHOR_LABEL,
      'id'   => WifiChartsConstants::TABLE_NETWORK_DATA_TABLE_ID,
    ),
    'device_os' => array(
      'name' => WifiChartsConstants::TABLE_DEVICE_OS_BREAKDOWN_ANCHOR_LABEL,
      'id'   => WifiChartsConstants::TABLE_DEVICE_OS_BREAKDOWN_TABLE_ID,
    ),
  /*    'connection_mode' => array(
      'name' => WifiChartsConstants::TABLE_CONNECTION_MODE_BREAKDOWN_TITLE,
      'id'   => WifiChartsConstants::TABLE_CONNECTION_MODE_BREAKDOWN_TABLE_ID,
    ),*/
    'connection_mode_time' => array(
      'name' => WifiChartsConstants::TABLE_CONNECTION_MODE_TIME_ANCHOR_LABEL,
      'id'   => WifiChartsConstants::TABLE_CONNECTION_MODE_TIME_TABLE_ID,
    ),
  );

  // All tables that will be displayed on the page.
  $tables_info = array(
    'network_data_time' => network_data($building_id, $tables['network_data_time']['id'], '', $show_data_days, FALSE),
    'device_os' => device_os_pie_chart($building_id, $tables['device_os']['id'], FALSE),
  // 'connection_mode' => connection_mode_pie_chart($building_id, $tables['connection_mode']['id'], FALSE),.
    'connection_mode_time' => connection_mode_line_chart($building_id, $tables['connection_mode_time']['id'], $show_data_days, FALSE),
  );

  if ($building_id === WifiChartsConstants::B_ID_UW) {
    $tables['radius_auth_time'] = array(
      'name' => WifiChartsConstants::TABLE_RADIUS_AUTH_ISSUES_ANCHOR_LABEL,
      'id'   => WifiChartsConstants::TABLE_RADIUS_AUTH_ISSUES_TABLE_ID,
    );
    $tables_info['radius_auth_time'] = radius_auth_chart($tables['radius_auth_time']['id'], $show_data_days, FALSE);
  }

  // Create Content for webpage.
  $content = array();
  // Top level links.
  $content[WifiChartsConstants::KEY_MENU_LINKS] = top_navigation_links();

  // List all current network info.
  foreach ($tables_info['network_data_time'] as $key => $value) {
    if (strpos($key, WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX) !== FALSE) {
      $content['current'][$key] = $value;
    }
  }
  if ($building_id === WifiChartsConstants::B_ID_UW) {
    $content['current']['auth_issues' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX] =
      $tables_info['radius_auth_time']['auth_issues' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX];
  }
  $content[WifiChartsConstants::KEY_LAST_UPDATED] = $tables_info['network_data_time'][WifiChartsConstants::KEY_LAST_UPDATED];

  // Create list of URL Anchors.
  $content['anchor_link_list'] = anchor_link_list($tables);

  // Link back to original charts.
  $content['links']['link_to_charts'] = create_hyperlink_content(sprintf(WifiChartsConstants::LINK_CHARTS_BUILDING, $building_id), WifiChartsConstants::VIEW_CHARTS);

  // If Rogues are enabled, display link to Rogues graph.
  if ($show_rogues && $building_id === WifiChartsConstants::B_ID_UW) {
    $content['links']['link_to_rogues'] = create_hyperlink_content(
      sprintf(WifiChartsConstants::LINK_ROGUE_APS, $building_id), WifiChartsConstants::GO_TO_ROGUE_CHARTS, '', '<br /> <br />');
  }

  // Initialize charts.
  foreach ($tables as $table_name => $table_info) {
    $content['tables'][$table_name]['table'] = array();
    $content['tables'][$table_name]['back_to_top'] = ref_anchor(
      WifiChartsConstants::ANCHOR_LIST,
      WifiChartsConstants::BACK_TO_TOP,
      '<p>', '</p>',
      WifiChartsConstants::CLASS_BACK_TO_TOP
    );
  }

  // Network data table.
  $content['tables']['network_data_time']['table'] = $tables_info['network_data_time']['data'];
  // Device OS breakdown table.
  $content['tables']['device_os']['last_updated'] = $tables_info['device_os'][WifiChartsConstants::KEY_LAST_UPDATED];
  $content['tables']['device_os']['table']        = $tables_info['device_os']['data'];
  /*  // Connection mode table. Redundant with the table below.
  $content['tables']['connection_mode']['last_updated'] = $tables_info['connection_mode'][WifiChartsConstants::KEY_LAST_UPDATED];
  $content['tables']['connection_mode']['table'] = $tables_info['connection_mode']['data'];
   */
  // Connection mode over time table.
  $content['tables']['connection_mode_time']['table'] = $tables_info['connection_mode_time']['data'];
  // RADIUS Authentication Issues Table.
  if ($building_id === WifiChartsConstants::B_ID_UW) {
    $content['tables']['radius_auth_time']['table'] = $tables_info['radius_auth_time']['data'];
  }

  $content['about_info'] = about_label();

  $content['#attached']['css'] = array(
    drupal_get_path('module', 'uw_wifi_charts') . WifiChartsConstants::CSS_TABLE_ALIGN_RIGHT => array(),
  );
  return $content;
}

/**
 * Page callback function for wifi-charts/1/rogues.
 *
 * @see hook_menu()
 */
function uw_wifi_charts_rogues() {
  // Get WiFi Charts settings.
  $show_rogues = variable_get(WifiChartsConstants::CONFIG_ROGUES_ENABLED,
  // Rogue AP's chart enabled?
    WifiChartsConstants::CONFIG_DEFAULT_ROGUES_ENABLED);
  $show_data_days = variable_get(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS,
  // How many days to show data in the charts.
    WifiChartsConstants::CONFIG_DEFAULT_SHOW_DATA_DAYS);

  // Check if show_rogues is TRUE or FALSE.
  if ($show_rogues) {

    // Get markup for rogue data.
    $rogues_data = rogues_data(WifiChartsConstants::CHART_ROGUE_AP_TIME_ANCHOR_ID, $show_data_days, TRUE);

    $content = array();
    // Top level links.
    $content[WifiChartsConstants::KEY_MENU_LINKS] = top_navigation_links();

    // Generate content renderables.
    $content['current']['rogues' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX] =
    // Current number of rogue AP's.
      $rogues_data['rogues' . WifiChartsConstants::KEY_CURRENT_DATA_SUFFIX];

    // When the data was last updated.
    $content[WifiChartsConstants::KEY_LAST_UPDATED] = $rogues_data[WifiChartsConstants::KEY_LAST_UPDATED];

    $content['links']['link_to_tables'] = create_hyperlink_content(
    // View data as table.
      WifiChartsConstants::LINK_ROGUE_TABLE, WifiChartsConstants::VIEW_TABLES_ROGUE, '<br /> <p>', '</p>');

    // JS is disabled markup.
    $content['no_js'] = no_js_markup();

    // Rogue AP's chart.
    $content['charts']['rogues_over_time']['chart'] = $rogues_data['chart'];

    // Information on how rogue data is updated.
    $content['about_info_rogues'] = about_label_rogues();

    return $content;
  }
  else {
    display_404();
  }
}

/**
 * Page callback function for wifi-charts/building/1/rogues/data.
 *
 * @see hook_menu()
 */
function uw_wifi_charts_rogues_table() {
  // Get WiFi Charts settings.
  $show_rogues = variable_get(WifiChartsConstants::CONFIG_ROGUES_ENABLED,
  // Rogue AP's chart enabled?
    WifiChartsConstants::CONFIG_DEFAULT_ROGUES_ENABLED);
  $show_data_days = variable_get(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS,
  // How many days to show data in the charts.
    WifiChartsConstants::CONFIG_DEFAULT_SHOW_DATA_DAYS);

  // Check if show_rogues is TRUE or FALSE.
  if ($show_rogues) {

    $content = array();
    // Top level links.
    $content[WifiChartsConstants::KEY_MENU_LINKS] = top_navigation_links();

    // Get markup for rogue data.
    $rogues_data = rogues_data(WifiChartsConstants::TABLE_ROGUES_TABLE_ID, $show_data_days, FALSE);

    $content['links']['link_to_charts'] = create_hyperlink_content(
    // Link to view data in chart form.
      WifiChartsConstants::LINK_ROGUE_APS, WifiChartsConstants::VIEW_CHARTS_ROGUE, '<br /> <p>', '</p>');

    // Table of rogue AP data.
    $content['tables']['rogues_over_time']['table'] = $rogues_data['data'];

    // CSS file to align table columns to the right, except the first which is center-aligned.
    $content['#attached']['css'] = array(
      drupal_get_path('module', 'uw_wifi_charts') . WifiChartsConstants::CSS_TABLE_ALIGN_RIGHT => array(),
    );

    return $content;
  }
  else {
    display_404();
  }
}
