<?php

/**
 * @file
 * It provides hyperlink to the building-specific page
 * that displays more in depth data.
 */

require_once __DIR__ . '/uw_wifi_charts.constants.inc';
require_once __DIR__ . '/uw_wifi_charts.page_content.inc';

/**
 * Page callback function for wifi-charts/building-select-index.
 *
 * @see hook_menu()
 * This is an accessible version of the Google Maps page.
 */
function uw_wifi_charts_building_select_index() {
  // Header row.
  $header = array(
    array(
      'data' => t(WifiChartsConstants::BUILDING_INDEX_HEADER_B_NAME),
      'abbr' => WifiChartsConstants::BUILDING_INDEX_HEADER_B_NAME_ABBR,
    ),
    array(
      'data' => t(WifiChartsConstants::BUILDING_INDEX_HEADER_LOC),
      'abbr' => WifiChartsConstants::BUILDING_INDEX_HEADER_LOC_ABBR,
    ),
    array(
      'data' => t(WifiChartsConstants::BUILDING_INDEX_HEADER_CLIENTS),
      'abbr' => WifiChartsConstants::BUILDING_INDEX_HEADER_CLIENTS_ABBR,
    ),
    array(
      'data' => t(WifiChartsConstants::BUILDING_INDEX_HEADER_DOWNLOAD),
      'abbr' => WifiChartsConstants::BUILDING_INDEX_HEADER_DOWNLOAD_ABBR,
    ),
    array(
      'data' => t(WifiChartsConstants::BUILDING_INDEX_HEADER_UPLOAD),
      'abbr' => WifiChartsConstants::BUILDING_INDEX_HEADER_UPLOAD_ABBR,
    ),
  );
  $content = array();
  $content[WifiChartsConstants::KEY_MENU_LINKS] = top_navigation_links();
  db_set_active(WifiChartsConstants::WIFI_CHARTS_KEY);

  try {
    // Query the database for the latest network data available
    // for each building and the time of submission.
    $building_data_query = db_query(
      'SELECT b.BUILDING_ID AS b_id, b.BUILDING_CODE AS b_code, ' .
        'b.BUILDING_NAME AS b_name, b.LATITUDE AS b_lat, ' .
        'b.LONGITUDE AS b_long, d.SUBMISSION_DATETIME AS d_date, ' .
        'd.CLIENTS AS d_clients, d.DOWNLOAD_RATE_BPS AS d_down, ' .
        'd.UPLOAD_RATE_BPS AS d_up ' .
      'FROM {BUILDING_DATA} d ' .
      'INNER JOIN {BUILDINGS} b ' .
        'ON b.BUILDING_ID = d.BUILDING_ID ' .
      'WHERE d.SUBMISSION_DATETIME = ' .
        '(SELECT MAX(SUBMISSION_DATETIME) FROM {BUILDING_DATA}) ' .
        'AND b.IS_BUILDING = TRUE ' .
      'ORDER BY b_name ASC'
    );
  }
  catch (Exception $e) {
    db_set_active();
    $content['table'] = data_table_content(
      WifiChartsConstants::BUILDING_INDEX_TABLE_ID,
      t(WifiChartsConstants::BUILDING_INDEX_TITLE),
      $header,
      arr_to_drupal_table_arr(array()),
      WifiChartsConstants::TABLE_DEFAULT_CLASS
    );
    return $content;
  }
  db_set_active();
  // Check if result query is empty.
  if (!$building_data_query->rowCount()) {
    $content['table'] = data_table_content(
      WifiChartsConstants::BUILDING_INDEX_TABLE_ID,
      t(WifiChartsConstants::BUILDING_INDEX_TITLE),
      $header,
      arr_to_drupal_table_arr(array()),
      WifiChartsConstants::TABLE_DEFAULT_CLASS
    );
    return $content;
  }

  $building_data_arr = array();
  if (!empty($building_data_query)) {
    // Retrieve result set as indexed array of rows as stdObjects.
    $building_data_arr = $building_data_query->fetchAll();
  }

  if (isset($building_data_arr[1])) {
    // Creates markup for when the data was last updated.
    $content[WifiChartsConstants::KEY_LAST_UPDATED] = date_last_updated_content($building_data_arr[1]->d_date, '<p>', '</p>');
  }

  $building_rows = array();
  // Iterate through each row in the database to create rows for a table.
  foreach ($building_data_arr as $building) {

    // Setting up variables so code is more readable.
    $b_id       = $building->b_id;
    $b_name     = $building->b_code;
    $b_lat      = $building->b_lat;
    $b_long     = $building->b_long;
    $d_clients  = $building->d_clients;
    $d_download = data_rate_prettify($building->d_down);
    $d_upload   = data_rate_prettify($building->d_up);

    // If building_name exists, use it.
    if ($building->b_name !== '') {
      $b_name = $building->b_name;
    }

    // If no location data found display Unavailable.
    if ($b_lat === NULL || $b_long === NULL) {
      $lat_long = WifiChartsConstants::BUILDING_LOC_UNAVAILABLE;
    }
    else {
      $lat_long = $b_lat . ', ' . $b_long;
    }

    // Add another array to $building_rows.
    $building_rows[] = array(
      'data' => array(
        '<a href="' . url(WifiChartsConstants::BUILDING_INDEX_BUILDING_URL . $b_id) . '">' . $b_name . '</a>',
        $lat_long,
        array(
          'data' => $d_clients,
        ),
        array(
          'data' => $d_download,
        ),
        array(
          'data' => $d_upload,
        ),
      ),
      'no_striping' => TRUE,
    );
  }

  // Create table.
  $content['table'] = data_table_content(
    WifiChartsConstants::BUILDING_INDEX_TABLE_ID, t(WifiChartsConstants::BUILDING_INDEX_TITLE), $header, $building_rows, WifiChartsConstants::TABLE_DEFAULT_CLASS);
  $content['#attached']['css'] = array(
  // Center align the left most column, right aligning the rest.
    drupal_get_path('module', 'uw_wifi_charts') . WifiChartsConstants::CSS_TABLE_ALIGN_RIGHT => array(),
  );
  return $content;
}


