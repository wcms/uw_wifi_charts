<?php

/**
 * @file
 * This file contains Drupal Hooks for Wifi Charts administration and configuration content.
 * Add new WiFi Charts settings/permissions here.
 */

require_once 'uw_wifi_charts.constants.inc';

/**
 * Form callback function.
 */
function uw_wifi_charts_admin_settings_form($form, &$form_state) {
  $form = array();

  // Title for database settings.
  $form['settings'] = array(
    '#type'        => 'markup',
    '#markup' => '<h2>' . t(WifiChartsConstants::SETTINGS_DISPLAY_DB_TITLE) . '</h2><br />',
  );

  // Description of the page.
  $form['overview'] = array(
    '#markup' => t(WifiChartsConstants::SETTINGS_OVERVIEW_DESCRIPTION),
  );

  // Checkbox: Enable charts that display the number of rogues/suspected rogues.
  $form[WifiChartsConstants::CONFIG_ROGUES_ENABLED] = array(
    '#title'         => t(WifiChartsConstants::SETTINGS_ENABLE_ROGUE_CHART),
    '#description'   => t(WifiChartsConstants::SETTINGS_ENABLE_ROGUE_CHART_DESC),
    '#type'          => 'checkbox',
    '#default_value' => variable_get(WifiChartsConstants::CONFIG_ROGUES_ENABLED,
      WifiChartsConstants::CONFIG_DEFAULT_ROGUES_ENABLED),
  );

  // Textfield: Number of days to display data. (This is for the per building pages.)
  $form[WifiChartsConstants::CONFIG_SHOW_DATA_DAYS] = array(
    '#title'         => t(WifiChartsConstants::SETTINGS_NUM_DAYS_DISPLAY_DATA),
    '#description'   => t(WifiChartsConstants::SETTINGS_NUM_DAYS_DISPLAY_DATA_DESC),
    '#type'          => 'textfield',
    '#default_value' => variable_get(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS,
      WifiChartsConstants::CONFIG_DEFAULT_SHOW_DATA_DAYS),
    '#required'      => TRUE,
    '#size'          => 9,
    '#maxlength'     => 3,
  );

  // Textfield: Number of days to display data on the cover page.
  $form[WifiChartsConstants::CONFIG_SHOW_DATA_DAYS_COVER] = array(
    '#title'         => t(WifiChartsConstants::SETTINGS_NUM_DAYS_DISPLAY_DATA_COVER),
    '#description'   => t(WifiChartsConstants::SETTINGS_NUM_DAYS_DISPLAY_DATA_COVER_DESC),
    '#type'          => 'textfield',
    '#default_value' => variable_get(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS_COVER,
      WifiChartsConstants::CONFIG_DEFAULT_SHOW_DATA_DAYS_COVER),
    '#required'      => TRUE,
    '#size'          => 9,
    '#maxlength'     => 3,
  );

  // $form['#submit'][] = 'uw_wifi_charts_admin_settings_form_submit';     // Call this function to submit.
  //  $form['#tree'] = TRUE; // Keep $form the same structure as it was returned here.
  // Allows Drupal to handle form submission and configuration storage.
  return system_settings_form($form);

}

/**
 * Validates WiFi Charts admin settings.
 */
function uw_wifi_charts_admin_settings_form_validate($form, &$form_state) {
  // Shorthand for long array names.
  //  $show_data = variable_get(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS);
  //  $show_data_cover_page = variable_get(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS_COVER);.
  $show_data = $form_state['values'][WifiChartsConstants::CONFIG_SHOW_DATA_DAYS];
  $show_data_cover_page = $form_state['values'][WifiChartsConstants::CONFIG_SHOW_DATA_DAYS_COVER];

  // Regular Expression for positive integers.
  $pos_int_regex = '/^[1-9]\d*$/';

  // Validate keep_data data type and range.
  if (!preg_match($pos_int_regex, $show_data)) {
    // Display error message. Do not save submitted data.
    form_set_error(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS, t(WifiChartsConstants::SETTINGS_INVALID_NUM_DAYS_POS_INT));
  }
  if (!preg_match($pos_int_regex, $show_data_cover_page)) {
    // Display error message. Do not save submitted data.
    form_set_error(WifiChartsConstants::CONFIG_SHOW_DATA_DAYS_COVER, t(WifiChartsConstants::SETTINGS_INVALID_NUM_DAYS_POS_INT));
  }
}
